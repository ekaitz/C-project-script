#!/bin/bash

DIRNAME=$1
if [ -z $DIRNAME ]
then
	echo -e "\e[31m\e[1m** ERROR **\e[0m  You need to specify the project name"
	exit 1
fi

if [ -d $DIRNAME ]
then
	echo -e "\e[31m\e[1m** ERROR **\e[0m  Project alredy exists"
	exit 1
fi

mkdir -p $DIRNAME/src/obj
mkdir -p $DIRNAME/lib
mkdir -p $DIRNAME/include
cp ~/scripts/C-project-script/makefile_template ./$DIRNAME/Makefile
